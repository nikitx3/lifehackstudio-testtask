package com.ex.javalifehack.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.ex.javalifehack.Constants;
import com.ex.javalifehack.R;
import com.ex.javalifehack.pogo.Company;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.CompanyViewHolder> {

    private List<Company> companyList;
    private OnPositionClickListener clickListener;

    public void setCompanyList(List<Company> companyList, OnPositionClickListener clickListener) {
        this.companyList = companyList;
        this.clickListener = clickListener;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CompanyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main, parent, false);
        return new CompanyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CompanyViewHolder holder, int position) {
        holder.bind(companyList.get(position));
    }

    @Override
    public int getItemCount() {
        return companyList.size();
    }

    class CompanyViewHolder
            extends RecyclerView.ViewHolder {

        private TextView textViewCompany;
        private ImageView imageViewCompanyLogo;
        private CardView cardView;

        CompanyViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewCompany = itemView.findViewById(R.id.item_main_text_view_name);
            imageViewCompanyLogo = itemView.findViewById(R.id.item_main_image_view_logo);
            cardView = itemView.findViewById(R.id.item_main_card_view);
        }

        private void bind(Company company) {
            textViewCompany.setText(company.getName());

            Picasso.get()
                    .load(Constants.BASE_URL + company.getImg())
                    .fit()
                    .placeholder(R.drawable.ic_launcher_background) // preload
                    .error(R.drawable.ic_launcher_background) // load error
                    .centerCrop()
                    .into(imageViewCompanyLogo, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError(Exception e) {
                            e.printStackTrace();
                            e.getCause();
//                          Toast.makeText(MyApplication.getInstance(),
//                                    "Ошибка загрузки изображения компании",
//                                    Toast.LENGTH_SHORT).show();
                            Log.d(Constants.LOG_TAG + " <Main adapter>",
                                    "Ошибка загрузки изображения описания погоды: " + e);
                        }
                    });

            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickListener.onPositionClickListener(getAdapterPosition());
                }
            });
        }
    }
}
