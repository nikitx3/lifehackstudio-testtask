package com.ex.javalifehack.adapters;

public interface OnPositionClickListener {
    void onPositionClickListener(int position);
}
