package com.ex.javalifehack.network;

import io.reactivex.Observable;

import com.ex.javalifehack.pogo.Company;
import com.ex.javalifehack.pogo.CompanyDetail;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {

    //http://megakohz.bget.ru/test_task/test.php?id=ид_компании
    //http://megakohz.bget.ru/test_task/test.php

    @GET("test.php")
    Observable <List<Company>> getAllCompanies();

    @GET("test.php")
    Observable<List<CompanyDetail>> getCompanyByID(@Query("id") String companyName);
}
