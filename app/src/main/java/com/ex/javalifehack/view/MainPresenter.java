package com.ex.javalifehack.view;

import android.util.Log;

import com.ex.javalifehack.Constants;
import com.ex.javalifehack.network.ApiFactory;
import com.ex.javalifehack.network.ApiService;
import com.ex.javalifehack.pogo.Company;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

class MainPresenter {

    private CompositeDisposable compositeDisposable;
    private MainActivity view;
    private List<Company> companies;


    MainPresenter(MainActivity view) {
        this.view = view;
        companies = new ArrayList<Company>();
    }

    void loadData() {
        ApiFactory apiFactory = ApiFactory.getInstance();
        ApiService apiService = apiFactory.getApiService();
        compositeDisposable = new CompositeDisposable();
        Disposable disposable = apiService.getAllCompanies()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<Company>>() {
                    @Override
                    public void accept(List<Company> companyList) throws Exception {
                        view.showData(companyList);
                        companies = companyList;
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        view.showError(throwable);
                        Log.d(Constants.LOG_TAG, "error presenter " + throwable);
                    }
                });
        compositeDisposable.add(disposable);
    }

    void disposeDisposable() {
        if (compositeDisposable != null) {
            compositeDisposable.dispose();
        }
    }

    String getId(int position) {
        return companies.get(position).getId();
    }
}
