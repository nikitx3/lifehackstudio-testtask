package com.ex.javalifehack.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ex.javalifehack.Constants;
import com.ex.javalifehack.R;
import com.ex.javalifehack.pogo.CompanyDetail;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

public class DetailActivity extends AppCompatActivity {

    private String id;
    private DetailPresenter presenter;
    private TextView textViewTitle;
    private ImageView imageViewLogo;
    private TextView textViewDescription;
    private TextView textViewWebsite;
    private TextView textViewPhone;
    private Button buttonGoToMaps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initUI();
        getMyIntent();
        initDataBinding();
    }

    private void initUI() {
        setContentView(R.layout.activity_detail);
        textViewTitle = findViewById(R.id.detail_title);
        imageViewLogo = findViewById(R.id.detail_img);
        textViewDescription = findViewById(R.id.detail_description);
        textViewWebsite = findViewById(R.id.detail_website);
        textViewPhone = findViewById(R.id.detail_phone);
        buttonGoToMaps = findViewById(R.id.detail_location);
    }

    private void getMyIntent() {
        Intent intent = getIntent();
        id = intent.getStringExtra(Constants.INTENT_KEY);
    }

    private void initDataBinding() {
        presenter = new DetailPresenter(this);
        presenter.loadData();
    }

    public String getId() {
        return id;
    }

    public void showDetail(final List<CompanyDetail> detailList) {
        textViewTitle.setText(detailList.get(0).getName());
        Picasso.get()
                .load(Constants.BASE_URL + detailList.get(0).getImg())
                .fit()
                .placeholder(R.drawable.ic_launcher_background) // preload
                .error(R.drawable.ic_launcher_background) // load error
                .centerCrop()
                .into(imageViewLogo, new Callback() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onError(Exception e) {
                        e.printStackTrace();
                        e.getCause();
//                          Toast.makeText(MyApplication.getInstance(),
//                                    "Ошибка загрузки изображения компании",
//                                    Toast.LENGTH_SHORT).show();
                        Log.d(Constants.LOG_TAG + " <Detail activ>",
                                "Ошибка загрузки изображения описания погоды: " + e);
                    }
                });
        textViewDescription.setText(detailList.get(0).getDescription());
        textViewDescription.setText(detailList.get(0).getDescription());
        textViewWebsite.setText(detailList.get(0).getWww());
        textViewPhone.setText(detailList.get(0).getPhone());
        buttonGoToMaps.setOnClickListener(new View.OnClickListener() {
                                              @Override
                                              public void onClick(View v) {
                                                  goToMaps(detailList);
                                              }
                                          }
        );
    }

    private void goToMaps(List<CompanyDetail> detailList) {
        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + detailList.get(0).getLat() + "," + detailList.get(0).getLon());
        //Uri gmmIntentUri = Uri.parse("geo:" + detailList.get(0).getLat() + "," + detailList.get(0).getLon());
        Log.d(Constants.LOG_TAG + "target location", gmmIntentUri + "");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        //mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
        }
    }

    public void showError() {
        Toast.makeText(this, "Ошибка", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.disposeDisposable();
    }
}
