package com.ex.javalifehack.view;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ex.javalifehack.Constants;
import com.ex.javalifehack.R;
import com.ex.javalifehack.adapters.MainAdapter;
import com.ex.javalifehack.adapters.OnPositionClickListener;
import com.ex.javalifehack.pogo.Company;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements OnPositionClickListener {

    private RecyclerView recycler;
    private MainAdapter adapter;
    private MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initUI();
        setRecycler();
        initDataBinding();
    }

    private void initUI() {
        setContentView(R.layout.activity_main);
        recycler = findViewById(R.id.recycler_view_main);
    }

    private void setRecycler() {
        adapter = new MainAdapter();
        adapter.setCompanyList(new ArrayList<Company>(), this);
        recycler.setLayoutManager(new LinearLayoutManager(
                this,
                RecyclerView.VERTICAL,
                false
        ));
        recycler.setAdapter(adapter);
    }

    private void initDataBinding() {
        presenter = new MainPresenter(this);
        presenter.loadData();
    }

    @Override
    public void onPositionClickListener(int position) {
    }

    public void showData(List<Company> companyList) {
        adapter.setCompanyList(companyList, new OnPositionClickListener() {
            @Override
            public void onPositionClickListener(int position) {
                goToDetailActivity(position);
            }
        });
    }

    public void showError(Throwable throwable) {
        Toast.makeText(this, "Error " + throwable, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.disposeDisposable();
    }

    private void goToDetailActivity(int position) {
        Log.d(Constants.LOG_TAG, position + "position");
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra(Constants.INTENT_KEY, presenter.getId(position));
        startActivity(intent);
    }
}
