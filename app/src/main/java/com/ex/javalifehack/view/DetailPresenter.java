package com.ex.javalifehack.view;

import android.util.Log;
import android.widget.TextView;

import com.ex.javalifehack.Constants;
import com.ex.javalifehack.network.ApiFactory;
import com.ex.javalifehack.network.ApiService;
import com.ex.javalifehack.pogo.CompanyDetail;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

class DetailPresenter {

    private CompositeDisposable compositeDisposable;
    private DetailActivity view;

    DetailPresenter(DetailActivity view) {
        this.view = view;
    }

    void loadData() {
        ApiFactory apiFactory = ApiFactory.getInstance();
        ApiService apiService = apiFactory.getApiService();
        compositeDisposable = new CompositeDisposable();

        Disposable disposable = apiService.getCompanyByID(view.getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<CompanyDetail>>() {
                    @Override
                    public void accept(List<CompanyDetail> detailList) throws Exception {
                        view.showDetail(detailList);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        view.showError();
                        Log.d(Constants.LOG_TAG, "error presenter " + throwable);
                    }
                });
        compositeDisposable.add(disposable);
    }

    void disposeDisposable() {
        if (compositeDisposable != null) {
            compositeDisposable.dispose();
        }
    }
}
